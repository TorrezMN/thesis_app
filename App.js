// In App.js in a new project
import 'react-native-gesture-handler';

import * as React from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer, DarkTheme } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

// Fonts
import AppLoading from 'expo-app-loading';
import { useFonts } from 'expo-font';

// Importing Views
import Index from './views/Index';
import Menu from './views/Menu';
import Estadisticas from './views/Estadisticas';
import NuevoRegistro from './views/NuevoRegistro';
import Ayuda from './views/Ayuda';
import Config from './views/Config';
import Login from './views/Login';
import Register from './views/Register';

import ErrorRegistroUsuario from './views/ErrorRegistroUsuario';
import ErrorGenericoRegistroUsuarios from './views/ErrorGenericoRegistroUsuarios';
import RegistroUsuariosCorrecto from './views/RegistroUsuariosCorrecto';
import BacheRegistroCorrecto from './views/BacheRegistroCorrecto';
import BachesErrorRegistro from './views/BachesErrorRegistro';
import ErrorDatosUsuario from './views/ErrorDatosUsuario';

// Importing Redux
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import {store} from './store/store';


const Stack = createStackNavigator();


function App() {

  const [loaded] = useFonts({
    Raleway_Italic: require('./assets/fonts/Raleway-Italic.ttf'),
    Raleway_Light: require('./assets/fonts/Raleway-Light.ttf'),
  });


  if (!loaded) {
    return <AppLoading />;
  } else {
    
    return (
      <Provider store={store}>
        <NavigationContainer theme={DarkTheme}>
          <Stack.Navigator initialRouteName='Index' >
          {/* <Stack.Navigator initialRouteName='ActivityIndicator' > */}
            <Stack.Screen name='Index' component={Index} options={{ headerShown: false }} />
            <Stack.Screen name='Menu' component={Menu} options={{ title: 'Menu', headerShown: true }} />
            <Stack.Screen name='Estadisticas' component={Estadisticas} options={{ title: 'Estadisticas', headerShown: true }} />
            <Stack.Screen name='NuevoRegistro' component={NuevoRegistro} options={{ title: 'Nuevo Registro', headerShown: true }} />
            <Stack.Screen name='Ayuda' component={Ayuda} options={{ title: 'Ayuda', headerShown: true }} />
            <Stack.Screen name='Config' component={Config} options={{ title: 'Configuracion', headerShown: true }} />
            <Stack.Screen name='Login' component={Login} options={{ title: 'Login', headerShown: false }} />
            <Stack.Screen name='Register' component={Register} options={{  headerShown: false }} />
            
            {/* Errores */}
            <Stack.Screen name='ErrorRegistroUsuario' component={ErrorRegistroUsuario} options={{  headerShown: false }} />
            <Stack.Screen name='ErrorGenericoRegistroUsuarios' component={ErrorGenericoRegistroUsuarios} options={{  headerShown: false }} />
            <Stack.Screen name='ErrorDatosUsuario' component={ErrorDatosUsuario} options={{  headerShown: false }} />
            
              {/* Registro de Usuarios Correcto */}
            <Stack.Screen name='RegistroUsuariosCorrecto' component={RegistroUsuariosCorrecto} options={{  headerShown: false }} />
              {/* Registro de baches correcto. */}
            <Stack.Screen name='BacheRegistroCorrecto' component={BacheRegistroCorrecto} options={{  headerShown: false }} />
            <Stack.Screen name='BachesErrorRegistro' component={BachesErrorRegistro} options={{  headerShown: false }} />

          </Stack.Navigator>
        </NavigationContainer>
      </Provider>
    );
  }




}



export default App;