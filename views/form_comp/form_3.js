import React, { Component } from 'react';
import { View, Text, TextInput, StyleSheet, Modal, Dimensions, Button } from 'react-native';
import { connect, useSelector, useDispatch } from 'react-redux'
import {
  registroFormSetQuantity,
  registroFormSetDepth,
  registroFormSetDiameter
} from '../../store/actions'
import PropTypes from 'prop-types'
class Parte_3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cantidad: null,
      profundidad: null,
      diametro: null,
    };
  }

  render() {

    return (


      <View style={styles.container}>





        <Text style={styles.title}> Datos Fisicos </Text>
        <Text style={styles.subtitle}> {'Complete los datos segun corresponda.'} </Text>
        <Text style={styles.subtitle_important}> {'Ingrese las medidas en metros.'} </Text>
        <TextInput
          style={styles.textInputStyle}
          placeholder="Cantidad"
          placeholderTextColor="#60605e"
          numeric
          keyboardType={'numeric'}
          onChangeText={(value) => this.props.setQuantity(value)}
        // onChangeText={(value) => this.setState({ cantidad: value })}
        />
        <TextInput
          style={styles.textInputStyle}
          placeholder="Profundidad"
          placeholderTextColor="#60605e"
          numeric
          keyboardType={'numeric'}
          onChangeText={(value) => this.props.setDepth(value)}
        // onChangeText={(value) => this.setState({ profundidad: value })}
        />
        <TextInput
          style={styles.textInputStyle}
          placeholder="Diametro"
          placeholderTextColor="#60605e"
          numeric
          keyboardType={'numeric'}
          onChangeText={(value) => this.props.setDiameter(value)}
        // onChangeText={(value) => this.setState({ diametro: value })}
        />



      </View>


    );
  }
}




const mapDispatchToProps = (dispatch, val) => {
  return {
    setQuantity: (val) => dispatch(registroFormSetQuantity(val)),
    setDepth: (val) => dispatch(registroFormSetDepth(val)),
    setDiameter: (val) => dispatch(registroFormSetDiameter(val)),
  }
};

export default connect(null, mapDispatchToProps)(Parte_3)


// Styles
const window = Dimensions.get('window');
const screen = Dimensions.get('screen');


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: screen.height*0.4 ,

    marginBottom:(screen.height*0.20)*-1,

  },
  title: {
    fontFamily: 'Raleway_Light',
    color: '#FFFFFF',
    fontSize: 25,
    textAlign: 'center',
    marginTop: '-50%',

  },
  subtitle: {
    fontFamily: 'Raleway_Light',
    color: '#FFFFFF',
    fontSize: 15,
    textAlign: 'center',
  },
  subtitle_important: {
    fontFamily: 'Raleway_Light',
    color:'#EF6C00',
    fontSize: 15,
    textAlign: 'center',
    marginBottom: 30,
    
  },
  textInputStyle: {
    width: screen.width * 0.7,
    color:'#FFFFFF',
    padding: 5,
    marginTop: 5,
    borderRadius: 5,
    fontFamily: 'Raleway_Light',
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#EF6C00',
  },

});