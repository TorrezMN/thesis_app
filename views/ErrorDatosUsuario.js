import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
// Importing Waves
import BottomWave from '../assets/svg_pats/bottom_wave';
// Styles
const window = Dimensions.get('window');
const screen = Dimensions.get('screen');






class ErrorDatosUsuario extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <Text style={styles.page_title}> Error - Datos de Usuarios </Text>
                <Text style={styles.page_descripcion}> {'No se encontraron datos de usuario registrado. Por favor ingrese o cree una cuenta nueva.'} </Text>

                <View style={styles.botonera}>

                <TouchableOpacity style={styles.button_aceptar} onPress={() => navigate('Login')}>
                    <Text style={styles.button_text}>Login</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.button_aceptar} onPress={() => navigate('Register')}>
                    <Text style={styles.button_text}>Registrarse</Text>
                </TouchableOpacity>
                </View>
                <BottomWave />
            </View>
        );
    }
}

export default ErrorDatosUsuario;



const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        width: screen.width,
        height: screen.height,
    },
    page_title: {
        width: screen.width * 0.80,
        color: '#FFFFFF',
        backgroundColor: 'red',
        padding: 5,
        fontFamily: 'Raleway_Light',
        textAlign: 'left',
    },
    page_descripcion: {
        textAlign: 'left',
        fontFamily: 'Raleway_Light',
        marginTop: 4,
        width: screen.width * 0.80,
        color: '#FFFFFF',
    },
    botonera:{
        flex:1,
        flexDirection:'row', 
        justifyContent:'center', 
        alignItems:'center',
        minWidth:screen.width*0.80,
        maxWidth:screen.width*0.80,
        
        minHeight:screen.height*0.20,
        maxHeight:screen.height*0.20,
        

    },
    button_text:{
        color:'#EF6C00',
        textAlign:'center',
    },
    button_aceptar:{
        flex:1,
        flexDirection:'column',
        justifyContent:'center',
        alignItems:'center',
        
        borderWidth:1,
        borderColor:'#EF6C00',
        borderRadius:5,
        padding: 10,
        margin:'10%',
        maxWidth:screen.width*0.35,
        minWidth:screen.width*0.35,
        maxHeight:screen.height*0.10,
    }
})