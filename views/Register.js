import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TextInput, TouchableOpacity } from 'react-native';
import { connect, useSelector, useDispatch } from 'react-redux'
import { register_new_user } from '../store/actions';
import { AntDesign } from '@expo/vector-icons';
const axios = require('axios');
// Importing Waves
import BottomWave from '../assets/svg_pats/bottom_wave';
class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: null,
      password1: null,
      password2: null,
      password_ok: false,
      email_ok: false,
    };
  }
  check_email() {
    const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(this.state.email) === true) {
      this.setState({ email_ok: true })
      console.log("EMAIL OK!")
    }
    else {
      this.setState({ email_ok: false })
      console.log("EMAIL NOT OK!")
    }
  }
  check_passwords() {
    if (this.state.password1.length > 8 && this.state.password2.length > 8) {
      if (this.state.password1 === this.state.password2) {
        this.setState({ password_ok: true })
      } else {
        this.setState({ password_ok: false })
      }
    } else {
      this.setState({ password_ok: false })
    }
  }
  register_user() {
    const { navigate } = this.props.navigation;
    this.props.registrar_usuario(this.state,navigate);
    this.check_email();
    this.check_passwords();
    if(this.state.email_ok && this.state.password_ok){
      console.log('TODO OK!')
    }else{
      console.log(this.state)
    }
  }
  render() {
    return (
      <View style={styles.register_container} >
        <Text style={styles.view_title}><AntDesign name="login" size={24} color="#EF6C00" /> Registrarse </Text>
        <View style={styles.input_container} >
          <TextInput
            style={styles.input_item}
            placeholder="Email"
            placeholderTextColor="#FFFFFF"
            autoComplete={false}
            autoCapitalize="none"
            keyboardType={'email-address'}
            onChangeText={(value) => this.setState({ email: value })}
            onBlur={() => this.check_email()}
          />
          <TextInput
            style={styles.input_item}
            placeholder="Contraseña"
            placeholderTextColor="#FFFFFF"
            secureTextEntry={true}
            autoComplete={false}
            autoCapitalize="none"
            keyboardType={'default'}
            onChangeText={(value) => this.setState({ password1: value })}
          />
          <TextInput
            style={styles.input_item}
            placeholder="Contraseña"
            placeholderTextColor="#FFFFFF"
            secureTextEntry={true}
            autoComplete={false}
            autoCapitalize="none"
            keyboardType={'default'}
            onChangeText={(value) => this.setState({ password2: value })}
            onBlur={() => this.check_passwords()}
          />
          <TouchableOpacity style={styles.button_aceptar} onPressIn={() => this.check_passwords()} onPress={() => this.register_user()}>
            <Text style={styles.button_text}><AntDesign name="checkcircleo" size={24} color="#FFFFFF" /> Aceptar</Text>
          </TouchableOpacity>
        </View>
        <BottomWave />
      </View>
    );
  }
}
const mapDispatchToProps = (dispatch, val,nav) => {
  return {
    registrar_usuario: (val,nav) => dispatch(register_new_user(val,nav)),
  }
};

export default connect(null, mapDispatchToProps)(Register)
// Styles
const window = Dimensions.get('window');
const screen = Dimensions.get('screen');
const styles = StyleSheet.create({
  register_container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: screen.width,
    height: screen.height,
    minHeight: screen.height * 0.80,
  },
  view_title: {
    color: '#EF6C00',
    fontSize: 24,
    fontFamily: 'Raleway_Light',
    textAlign: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '-25%',
    // backgroundColor:'pink',
    width: screen.width * 0.75,
    maxHeight: screen.height * 0.10,
    borderBottomWidth: 1,
    borderBottomColor: '#EF6C00',
  },
  input_container: {
    marginTop: '20%',
    width: screen.width * 0.75,
    maxHeight: screen.height * 0.40,
    flex: 1,
    // backgroundColor:'red',
  },
  input_item: {
    color: '#EF6C00',
    fontFamily: 'Raleway_Light',
    borderWidth: 1,
    borderColor: '#EF6C00',
    borderRadius: 5,
    paddingLeft: 10,
    marginTop: 5,
  },
  button_aceptar: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#EF6C00',
    borderRadius: 5,
    width: screen.width * 0.75,
    maxHeight: screen.height * 0.10,
    marginTop: '10%',
    minHeight: screen.height * 0.10,
  },
  button_text: {
    fontFamily: 'Raleway_Light',
    marginLeft: '20%',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 24,
    color: '#FFFFFF',
  }
});