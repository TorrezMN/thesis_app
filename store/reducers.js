import { Settings } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
    RESET_FORM,
    RESET_FORM_DATA,
    FORM_NEXT_STEP,
    FORM_PREVIOUS_STEP,
    REGISTRO_FORM_SET_TIPO_CAMINO,
    REGISTRO_FORM_SET_TIPO_MATERIAL,
    REGISTRO_FORM_SET_TIPO_TRAFICO,
    REGISTRO_FORM_SET_QUANTITY,
    REGISTRO_FORM_SET_DEPTH,
    REGISTRO_FORM_SET_DIAMETER,
    REGISTRO_FORM_SET_COORDINATES,
    REGISTER_USER,
    SET_USER_DATA,
    LOGIN_USER,
    REGISTRO_FORM_SAVE,
} from './actionTypes';
const axios = require('axios');
const initialState = {
    formulario_registro_nuevo: {
        part: 0,
    },
    registro_form_data: {
        diameter: null,
        depth: null,
        amount: null,
        tipo_camino: null,
        tipo_material: null,
        tipo_trafico: null,
        coordenadas: null,
    },
    user: {
        logedIn: false,
        registered: false,
        user_data: null,
    }
};
const get_user_data = async () => {
    try {
        const value = await AsyncStorage.getItem('@user_data')
        if (value !== null) {
            return (JSON.parse(value));
        }
    } catch (e) {
        console.log(e);
    }
}
const store_user_data = async (value, nav) => {
    try {
        await AsyncStorage.setItem('@user_data', value)
    } catch (e) {
        nav('ErrorGenericoRegistroUsuarios');
    }
}
export const mainReducer = (state = initialState, action) => {
    switch (action.type) {
        case RESET_FORM:
            return {
                ...state,
                formulario_registro_nuevo: {
                    ...state.formulario_registro_nuevo,
                    part: 0
                }
            };
        case RESET_FORM_DATA:
            return {
                ...state,
                registro_form_data: {
                    ...state.registro_form_data,
                    diameter: null,
                    depth: null,
                    amount: null,
                    tipo_camino: null,
                    tipo_material: null,
                    tipo_trafico: null,
                    coordenadas: null,
                }
            };
        case FORM_NEXT_STEP:
            return {
                ...state,
                formulario_registro_nuevo: {
                    ...state.formulario_registro_nuevo,
                    part: state.formulario_registro_nuevo.part + 1
                }
            }
        case FORM_PREVIOUS_STEP:
            return {
                ...state,
                formulario_registro_nuevo: {
                    ...state.formulario_registro_nuevo,
                    part: state.formulario_registro_nuevo.part - 1
                }
            }
        // New Form Redurse Registration
        case REGISTRO_FORM_SET_TIPO_CAMINO:
            return {
                ...state,
                registro_form_data: {
                    ...state.registro_form_data,
                    tipo_camino: action.val
                }
            }
        case REGISTRO_FORM_SET_TIPO_MATERIAL:
            return {
                ...state,
                registro_form_data: {
                    ...state.registro_form_data,
                    tipo_material: action.val
                }
            }
        case REGISTRO_FORM_SET_TIPO_TRAFICO:
            return {
                ...state,
                registro_form_data: {
                    ...state.registro_form_data,
                    tipo_trafico: action.val
                }
            }
        case REGISTRO_FORM_SET_QUANTITY:
            return {
                ...state,
                registro_form_data: {
                    ...state.registro_form_data,
                    amount: action.val
                }
            }
        case REGISTRO_FORM_SET_DEPTH:
            return {
                ...state,
                registro_form_data: {
                    ...state.registro_form_data,
                    depth: action.val
                }
            }
        case REGISTRO_FORM_SET_DIAMETER:
            return {
                ...state,
                registro_form_data: {
                    ...state.registro_form_data,
                    diameter: action.val
                }
            }
        case REGISTRO_FORM_SET_COORDINATES:
            return {
                ...state,
                registro_form_data: {
                    ...state.registro_form_data,
                    coordenadas: action.val
                }
            }
        case REGISTER_USER:
            let BASE_URL_REGISTER_USER = "https://baches-thesis.herokuapp.com/api_V1/register_user"
            let data =
            {
                'email': action.val.email,
                'password': action.val.password1,
            }
            axios.post(BASE_URL_REGISTER_USER, data)
                .then(function (response) {
                    store_user_data(JSON.stringify(response.data), action.nav)
                    action.nav('RegistroUsuariosCorrecto');
                })
                .catch(function (error) {
                    action.nav('ErrorGenericoRegistroUsuarios');
                });
            return {
                ...state,
                user: {
                    ...state.user,
                    registered: true
                }
            }
        case LOGIN_USER:
            console.log('LOGIN USER REDUCER!')
            let BASE_URL_LOGIN_USER = "https://baches-thesis.herokuapp.com/api_V1/login_user"
            let my_user_data = {
                'password': action.val.password,
                'username': action.val.email,
            };
            console.log('USER_DATA', my_user_data)
            axios.post(BASE_URL_LOGIN_USER, my_user_data)
                .then(function (response) {
                    store_user_data(JSON.stringify(response.data), action.nav)
                    action.nav('RegistroUsuariosCorrecto');
                })
                .catch(function (error) {
                    console.log('ERROR LOGIN: ', error);
                    action.nav('ErrorGenericoRegistroUsuarios');
                });
            return state
        case REGISTRO_FORM_SAVE:
            console.log("REGISTRO SAVE!");
            console.log('===============================================');
            let diameter = state.registro_form_data.diameter;
            let depth = state.registro_form_data.depth;
            let amount = state.registro_form_data.amount;
            let tipo_camino = state.registro_form_data.tipo_camino;
            let tipo_material = state.registro_form_data.tipo_material;
            let tipo_trafico = state.registro_form_data.tipo_trafico;
            let latitud = state.registro_form_data.coordenadas.latitude;
            let longitud = state.registro_form_data.coordenadas.longitude;
            let BASE_URL_REGISTER_POTHOLE = "https://baches-thesis.herokuapp.com/api_V1/registro_new";
            let my_data = {
                "pothole_diameter": diameter,
                "pothole_depth": depth,
                "pothole_quantity": amount,
                "material_type_road": tipo_material,
                "road_type": tipo_camino,
                "type_of_traffic": tipo_trafico,
                "pothole_coordinates": "SRID=4326;POINT (" + latitud + " " + longitud + ")",
                "registered_date": new Date().toDateString(),
                "user": action.user.user_id,
            }
            console.log('DATA_>>>', my_data)
            axios.post(BASE_URL_REGISTER_POTHOLE, my_data)
                .then(function (response) {
                    console.log('RESPONSE GUARDAR NUEVO REGISTRO!')
                    console.log(response)
                    action.nav('BacheRegistroCorrecto');
                })
                .catch(function (error) {
                    console.log('ERROR GUARDAR NUEVO REGISTRO!')
                    console.log(error)
                    action.nav('BachesErrorRegistro');
                });
            return state
        default:
            return state;
    }
}