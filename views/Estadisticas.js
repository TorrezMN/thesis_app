import React, { Component } from "react";
import { StyleSheet, Text, View, ScrollView } from "react-native";
import { AntDesign } from '@expo/vector-icons';
import { Feather } from '@expo/vector-icons';
import { Entypo } from '@expo/vector-icons';
import { Dimensions } from 'react-native';
// Importing Waves
import BottomWave from '../assets/svg_pats/bottom_wave';
import ActivityIndicator from './ActivityIndicator';
// Importando axios.
const axios = require('axios');
export default class Estadisticas extends Component {
  constructor(props) {
    super(props);
    this.base_urls = {
      api_status: 'https://baches-thesis.herokuapp.com/api_V1/api_status',
      api_user_stats: 'https://baches-thesis.herokuapp.com/api_V1/user_stats',
      api_registros_stats: 'https://baches-thesis.herokuapp.com/api_V1/registro_stats',
    };
    this.state = {
      isLoading: true,
      // API DATA
      apiStatus: null,
      apiUserStats: null,
      apiRegistroStats: null,
    };
  }
  // Helper Functions
  get_api_status() {
    return fetch(this.base_urls.api_status)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          isLoading: false,
          apiStatus: responseJson.version,
        })
      })
      .catch((error) => {
        console.log(error);
      })
  }
  get_api_user_status() {
    return fetch(this.base_urls.api_user_stats)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          isLoading: false,
          apiUserStats: {
            total_users: responseJson.users.total_users,
          },
        })
      })
      .catch((error) => {
        console.log(error);
      })
  }
  async componentDidMount() {
    // Make first two requests
    const [firstResponse, secondResponse, thirdResponse] = await Promise.all([
      axios.get(this.base_urls.api_status),
      axios.get(this.base_urls.api_user_stats),
      axios.get(this.base_urls.api_registros_stats),
    ]);
    this.setState({
      apiStatus: firstResponse.data.version,
      apiUserStats: secondResponse.data.users,
      apiRegistroStats: thirdResponse.data.registros,
      isLoading: false,
    });
  }
  render() {
    const { navigate } = this.props.navigation;
    if (this.state.isLoading) {
      return (
        <View style={styles.container}>
          <View style={styles.activity_indicator_container}>
            <ActivityIndicator/>
            {/* <ActivityIndicator size="large" color="#00ff00" /> */}
          </View>
          <BottomWave />
        </View>
      )
    } else {
      console.log('OBJECT KEYS', Object.keys(this.state.apiUserStats));
      return (
        <View style={styles.container}>
          <ScrollView style={styles.scrollView}>
            <View style={[styles.card_section_container, {marginTop:'20%'}]}>
              <Text style={styles.section_title}><AntDesign name="folderopen" size={30} color="#FFFFFF" /> Registros</Text>
              
              
                  <Text style={styles.section_data}> Total Registros : {this.state.apiRegistroStats.total_count} </Text>
              
              
              <View style={styles.subsection}>
                <Text style={styles.subsection_title}><AntDesign name="check" size={15} color="#EF6C00" /> Tipo de Material</Text>
                <Text style={styles.subsection_item}>Asfalto: {this.state.apiRegistroStats.tipo_material.asfalto}</Text>
                <Text style={styles.subsection_item}>Tierra: {this.state.apiRegistroStats.tipo_material.tierra}</Text>
                <Text style={styles.subsection_item}>Piedra: {this.state.apiRegistroStats.tipo_material.piedra}</Text>
              </View>
              <View style={styles.subsection}>
                <Text style={styles.subsection_title}><AntDesign name="check" size={15} color="#EF6C00" /> Tipo de Camino</Text>
                <Text style={styles.subsection_item}>Ruta: {this.state.apiRegistroStats.tipo_camino.ruta}</Text>
                <Text style={styles.subsection_item}>Vecinal: {this.state.apiRegistroStats.tipo_camino.vecinal}</Text>
                <Text style={styles.subsection_item}>Nacional: {this.state.apiRegistroStats.tipo_camino.nacional}</Text>
              </View>
              <View style={styles.subsection}>
                <Text style={styles.subsection_title}><AntDesign name="check" size={15} color="#EF6C00" /> Tipo de Trafico</Text>
                <Text style={styles.subsection_item}>Mucho: {this.state.apiRegistroStats.tipo_trafico.mucho}</Text>
                <Text style={styles.subsection_item}>Poco: {this.state.apiRegistroStats.tipo_trafico.poco}</Text>
                <Text style={styles.subsection_item}>Bajo: {this.state.apiRegistroStats.tipo_trafico.bajo}</Text>
              </View>
            </View>




            <View style={styles.card_section_container}>
              <Text style={styles.section_title}>
              <Feather name="users" size={30} color="#FFFFFF" /> Usuarios</Text>
              <Text style={styles.section_data}> Total Registros : {this.state.apiUserStats.total_users_count} </Text>
              <View style={styles.subsection}>
                <Text style={styles.subsection_title}><Entypo name="hour-glass" size={24} color="#EF6C00" /> Edades</Text>
                <Text style={styles.subsection_item}>Media: {this.state.apiUserStats.age.media}</Text>
                <Text style={styles.subsection_item}>Max: {this.state.apiUserStats.age.max}</Text>
                <Text style={styles.subsection_item}>Min: {this.state.apiUserStats.age.min}</Text>
              </View>
              <View style={styles.subsection}>
                <Text style={styles.subsection_title}><Entypo name="graduation-cap" size={24} color="#EF6C00" /> Nivel Educativo</Text>
                <Text style={styles.subsection_item}>Nivel Primario: {this.state.apiUserStats.education_level.primary}</Text>
                <Text style={styles.subsection_item}>Nivel Secundario: {this.state.apiUserStats.education_level.secondary}</Text>
                <Text style={styles.subsection_item}>Nivel Academico: {this.state.apiUserStats.education_level.academic}</Text>
              </View>
            </View>
          </ScrollView>
          <BottomWave />
        </View>
      );
    }
  }
}
// Styles
const window = Dimensions.get('window');
const screen = Dimensions.get('screen');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    position: 'relative',
    width: screen.width,
    height: screen.height,
    minHeight:screen.height*0.8,
  },
  activity_indicator_container: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  // CARD

  card_section_container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: screen.width,
    height: screen.height * 0.6,

    paddingBottom: '30%',



  },
  
  scrollView: {

  marginHorizontal: 20,
  width: screen.width,
  height: screen.height * 2,
  paddingBottom: '30%',
},
  section_title: {
  color: '#FFFFFF',
  width: screen.width,
  paddingLeft: '10%',
  fontFamily: 'Raleway_Light',
  fontSize: 30,
  // Border
  borderBottomWidth: 1,
  borderBottomColor: '#FFFFFF',
},
  section_data: {
  marginTop: 5,
  paddingTop: 3,
  paddingBottom: 3,
  color: '#FFFFFF',
},
  subsection_title: {
  // color: '#FFFFFF',
  color: '#EF6C00',
  fontFamily: 'Raleway_Light',
  fontSize: 15,
  textAlign: 'left',

},
  subsection: {
  marginLeft: '10%',
  width: screen.width,
  height: screen.height * 0.15,
  marginTop: 5,

},
  subsection_item: {
  color: '#FFFFFF',
  fontFamily: 'Raleway_Light',
  marginLeft: '10%',
}
});
