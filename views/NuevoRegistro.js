import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import { connect, useSelector, useDispatch } from 'react-redux'
import { nextStep, prevStep, resetForm, registroFormSetCoordinates, resetFormData, registroFormSave } from '../store/actions';
import { Dimensions } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
// Geo POS Imports
// import {Location} from 'expo';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';
// Importing Waves
import BottomWave from '../assets/svg_pats/bottom_wave';
// Importando partes del formulario.
import Parte_0 from './form_comp/form_0';
import Parte_1 from './form_comp/form_1';
import Parte_2 from './form_comp/form_2';
import Parte_3 from './form_comp/form_3';



class NuevoRegistro extends Component {
  constructor(props) {
    super(props);
    this.state = {
      location: null,
      geocode: null,
      saving:false,
      errorMessage: "",
      user_data: {
        user_id: null,
        user_token: null,
      },
    };
  }
  async get_user_data(nav) {
    try {
      const value = await AsyncStorage.getItem('@user_data')
      if (value !== null) {
        this.setState(
          {
            user_data: {
              user_id: JSON.parse(value).user_id,
              user_token: JSON.parse(value).user_token,
            },
          }
        );
      }else{
        nav('ErrorDatosUsuario');
      }
    } catch (e) {
      console.log(e);
    }
  }
  componentDidMount() {
    const { navigate } = this.props.navigation;
    this.get_user_data(navigate);
    this.getLocationAsync();
    
    // if (!this.props.user.registered) {
    //   navigate('Register');
    // }else{
    //    navigate('Login');
    //  }
  }
  getGeocodeAsync = async (location) => {
    let geocode = await Location.reverseGeocodeAsync(location)
    this.setState({ geocode })
  }
  getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        errorMessage: 'Permission to access location was denied',
      });
    }
    let location = await Location.getCurrentPositionAsync({ accuracy: Location.Accuracy.Highest });
    const { latitude, longitude } = location.coords
    this.getGeocodeAsync({ latitude, longitude })
    this.setState({ location: { latitude, longitude } });
    this.props.form_set_coordinates({ latitude, longitude });
  };
  render() {
    const { navigate } = this.props.navigation;
    if(!this.state.saving){

    
    switch (this.props.formulario_registro_nuevo.part) {
      case 0:
        // console.log(this.props.formulario_registro_nuevo.part);
        return (
          <View style={styles.container}>
            {/* <Text style={{ color: '#FFFFFF' }}>{JSON.stringify(this.state.location)}</Text> */}
            {/* <Text style={{ color: '#FFFFFF' }}>{JSON.stringify(this.state.geocode)}</Text> */}
            {/* <Text style={{ color: '#FFFFFF' }}>Parte: {this.props.formulario_registro_nuevo.part}</Text>  */}
            <Parte_0 />
            <View style={styles.form_botonera}>
              <TouchableOpacity style={styles.button} onPress={() => { this.props.prev_Step() }}>
                <Text style={styles.button_text}><AntDesign name="leftcircleo" size={20} color="#FFFFFF" /></Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.button_reset} onPress={() => { this.props.reset_Form(); this.props.reset_Form_Data(); this.setState({ location: null }); this.getLocationAsync(); }}>
                <Text style={styles.button_text}><Ionicons name="refresh-outline" size={20} color="#FFFFFF" /></Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.button} onPress={() => { this.props.next_Step() }}>
                <Text style={styles.button_text}> <AntDesign name="rightcircleo" size={20} color="#FFFFFF" /></Text>
              </TouchableOpacity>
            </View>
            <BottomWave />
          </View>
        )
      case 1:
        return (
          <View style={styles.container}>
            <Parte_1 />
            <View style={styles.form_botonera}>
              <TouchableOpacity style={styles.button} onPress={() => { this.props.prev_Step() }}>
                <Text style={styles.button_text}><AntDesign name="leftcircleo" size={20} color="#FFFFFF" /></Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.button_reset} onPress={() => { this.props.reset_Form() }}>
                <Text style={styles.button_text}><Ionicons name="refresh-outline" size={20} color="#FFFFFF" /></Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.button} onPress={() => { this.props.next_Step() }}>
                <Text style={styles.button_text}> <AntDesign name="rightcircleo" size={20} color="#FFFFFF" /></Text>
              </TouchableOpacity>
            </View>
            <BottomWave />
          </View>
        )
      case 2:
        // console.log(this.props.formulario_registro_nuevo.part);
        return (
          <View style={styles.container}>
            {/* <Text style={{ color: '#FFFFFF' }}>Nuevo Registro</Text>
            <Text style={{ color: '#FFFFFF' }}>Parte: {this.props.formulario_registro_nuevo.part}</Text> */}
            <Parte_2 />
            <View style={styles.form_botonera}>
              <TouchableOpacity style={styles.button} onPress={() => { this.props.prev_Step() }}>
                <Text style={styles.button_text}><AntDesign name="leftcircleo" size={20} color="#FFFFFF" /></Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.button_reset} onPress={() => { this.props.reset_Form() }}>
                <Text style={styles.button_text}><Ionicons name="refresh-outline" size={20} color="#FFFFFF" /></Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.button} onPress={() => { this.props.next_Step() }}>
                <Text style={styles.button_text}> <AntDesign name="rightcircleo" size={20} color="#FFFFFF" /></Text>
              </TouchableOpacity>
            </View>
            <BottomWave />
          </View>
        )
      case 3:
        // console.log(this.props.formulario_registro_nuevo.part);
        return (
          <View style={styles.container}>
            {/* <Text style={{ color: '#FFFFFF' }}>Nuevo Registro</Text>
            <Text style={{ color: '#FFFFFF' }}>Parte: {this.props.formulario_registro_nuevo.part}</Text> */}
            <Parte_3 />
            <View style={styles.form_botonera}>
              <TouchableOpacity style={styles.button} onPress={() => { this.props.prev_Step() }}>
                <Text style={styles.button_text}><AntDesign name="leftcircleo" size={20} color="#FFFFFF" /></Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.button_reset} onPress={() => { this.props.reset_Form() }}>
                <Text style={styles.button_text}><Ionicons name="refresh-outline" size={20} color="#FFFFFF" /></Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.button} onPress={() => { this.props.next_Step() }}>
                <Text style={styles.button_text}> <AntDesign name="rightcircleo" size={20} color="#FFFFFF" /></Text>
              </TouchableOpacity>
            </View>
            <BottomWave />
          </View>
        )
      case 4:
        let tipo_trafico = {
          1: "Mucho",
          2: "Poco",
          3: "Bajo",
        }
        let tipo_camino = {
          1: "Ruta",
          2: "Vecinal",
          3: "Nacional",
        }
        let tipo_material = {
          1: "Asfaltado",
          2: "Tierra",
          3: "Piedra",
        }
        return (
          <View style={styles.container}>
            <View style={styles.form_content}>
              <Text style={styles.form_content_text}>Cantidad: {this.props.formulario_data.amount} </Text>
              <Text style={styles.form_content_text}>Profundidad: {this.props.formulario_data.depth}</Text>
              <Text style={styles.form_content_text}>Diametro: {this.props.formulario_data.diameter}</Text>
              <Text style={styles.form_content_text}>Tipo de Camino: {tipo_camino[this.props.formulario_data.tipo_camino]}</Text>
              <Text style={styles.form_content_text}>Tipo de Material: {tipo_material[this.props.formulario_data.tipo_material]}</Text>
              <Text style={styles.form_content_text}>Tipo de Trafico: {tipo_trafico[this.props.formulario_data.tipo_trafico]}</Text>
            </View>
            {/* <Text style={{color:'#FFFFFF'}}>{this.state.user_data.user_id}</Text> */}
            {/* <Text style={{color:'#FFFFFF'}}>{this.state.user_data.user_token}</Text> */}
            <View style={styles.form_botonera}>
              <TouchableOpacity style={styles.confirm_button_reset} onPress={() => { this.props.reset_Form() }}>
                <Text style={styles.confirm_button_reset_text}><Ionicons name="refresh-outline" size={20} color="#FFFFFF" /></Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.button_confirm} onPress={() => {this.props.guardar_registro(navigate,this.state.user_data) }}>
                <Text style={styles.button_confirm_text}><AntDesign name="checkcircleo" size={20} color="#FFFFFF" /> Confirmar</Text>
              </TouchableOpacity>
            </View>
            {/* <View style={styles.form_botonera}>
                <TouchableOpacity style={styles.button} onPress={() => { this.props.prev_Step() }}>
                  <Text style={styles.button_text}><AntDesign name="leftcircleo" size={20} color="#FFFFFF" /></Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button_reset} onPress={() => { this.props.reset_Form() }}>
                  <Text style={styles.button_text}><Ionicons name="refresh-outline" size={20} color="#FFFFFF" /></Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button} onPress={() => { this.props.next_Step() }}>
                  <Text style={styles.button_text}> <AntDesign name="rightcircleo" size={20} color="#FFFFFF" /></Text>
                </TouchableOpacity>
              </View> */}
            <BottomWave />
          </View>
        )
      default:
        return (
          <View style={styles.container}>
            {/* <Text style={{ color: '#FFFFFF' }}>Nuevo Registro</Text>
            <Text style={{ color: '#FFFFFF' }}>Parte: {this.props.formulario_registro_nuevo.part}</Text> */}
            <View style={styles.form_botonera}>
              <TouchableOpacity style={styles.button} onPress={() => { this.props.reset_Form() }}>
                <Text style={styles.button_text}><Ionicons name="refresh-outline" size={20} color="#FFFFFF" /></Text>
              </TouchableOpacity>
            </View>
            <BottomWave />
          </View>
        )
    }
  }else{
    return(

      <ActivityIndicator/>
      )
  }
  }
}
const mapStateToProps = (state) => ({
  formulario_registro_nuevo: state.formulario_registro_nuevo,
  formulario_data: state.registro_form_data,
  user: state.user,
})
const mapDispatchToProps = (dispatch, val) => {
  return {
    next_Step: () => dispatch(nextStep()),
    prev_Step: () => dispatch(prevStep()),
    reset_Form: () => dispatch(resetForm()),
    reset_Form_Data: () => dispatch(resetFormData()),
    form_get_data: () => dispatch(registroFormGetData()),
    form_set_coordinates: (val) => dispatch(registroFormSetCoordinates(val)),
    guardar_registro: (nav,user) => dispatch(registroFormSave(nav,user)),
  }
};
export default connect(mapStateToProps, mapDispatchToProps)(NuevoRegistro)
// Styles
const window = Dimensions.get('window');
const screen = Dimensions.get('screen');
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    position: 'relative',
    width: screen.width,
    height: screen.height,
    minHeight: Math.round(Dimensions.get('window').height) * 0.7,
  },
  form_botonera: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: screen.width,
    height: screen.height * 0.2,
  },
  button: {
    borderWidth: 1,
    borderColor: '#EF6C00',
    borderRadius: 5,
    margin: 5,
    padding: 10,
    flex: 1,
    height: screen.height * 0.08,
    width: screen.width * 0.1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  button_reset: {
    borderWidth: 1,
    borderColor: '#EF6C00',
    borderRadius: 5,
    margin: 5,
    padding: 10,
    flex: 1,
    height: screen.height * 0.08,
    width: screen.width * 0.02,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  button_text: {
    color: '#FFFFFF',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 10,
    fontFamily: 'Raleway_Light',
    textAlign: 'center',
  },
  confirm_button_reset: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    maxWidth: screen.width * 0.20,
    minWidth: screen.width * 0.20,
    minHeight: screen.height * 0.10,
    maxHeight: screen.height * 0.10,
    borderWidth: 1,
    borderColor: '#2E7D32',
    borderRadius: 5,
    marginRight: 5,
  },
  confirm_button_reset_text: {
    fontSize: 20,
    color: '#FFFFFF',
    fontFamily: 'Raleway_Light',
    textAlign: 'center',
  },
  button_confirm: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    maxWidth: screen.width * 0.65,
    minHeight: screen.height * 0.10,
    maxHeight: screen.height * 0.10,
    borderWidth: 1,
    borderColor: '#2E7D32',
    borderRadius: 5,
  },
  button_confirm_text: {
    fontSize: 20,
    color: '#FFFFFF',
    fontFamily: 'Raleway_Light',
    textAlign: 'center',
  },
  form_content: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: '-15%',
    width: screen.width,
    height: screen.height * 0.50,
  },
  form_content_text: {
    fontSize: 20,
    color: '#FFFFFF',
    fontFamily: 'Raleway_Light',
    textAlign: 'center',
  }
});
