import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TextInput, TouchableOpacity } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import { connect, useSelector, useDispatch } from 'react-redux'
import { login_user } from '../store/actions';
// Styles
const window = Dimensions.get('window');
const screen = Dimensions.get('screen');

// Importing Waves
import BottomWave from '../assets/svg_pats/bottom_wave';


class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email:null,
      password:null,
    };
  }

  render() {
    const { navigate } = this.props.navigation;

    return (
      <View style={styles.menu_container}>
        <Text style={styles.login_title}><AntDesign name="login" size={24} color="#EF6C00" /> Login </Text>
        <View style={styles.input_container}>
          <TextInput
            style={styles.input_style}
            placeholder="Email"
            placeholderTextColor="#FFFFFF"
            autoComplete={false}
            autoCapitalize="none"



            keyboardType={'email-address'}
            
          onChangeText={(value) => this.setState({ email: value })}
          />
          <TextInput
            style={styles.input_style}
            placeholder="Contraseña"
            placeholderTextColor="#FFFFFF"
            secureTextEntry={true}

            autoComplete={false}
            keyboardType={'default'}
            
          onChangeText={(value) => this.setState({ password: value })}
          />
          <TouchableOpacity style={styles.login_button} onPress={() => this.props.login(this.state,navigate)}>
            <Text style={styles.login_button_text}><AntDesign name="checkcircleo" size={24} color="#FFFFFF" /> Aceptar</Text>
          </TouchableOpacity>
        </View>
        <BottomWave />

      </View>
    );
  }
}
const mapDispatchToProps = (dispatch, val,nav) => {
  return {
    login: (val,nav) => dispatch(login_user(val,nav)),
  }
};

export default connect(null, mapDispatchToProps)(Login)
 



const styles = StyleSheet.create({
  menu_container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    position: 'relative',
    width: screen.width,
    

    height:screen.height,
    minHeight:screen.height*0.80,
    

  },
  input_container: {

    
    maxHeight:screen.height*0.50,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  login_button_text: {
    color: '#FFFFFF',
    fontFamily: 'Raleway_Light',
    textAlign: 'center',
    paddingBottom: 3,
    paddingTop: 3,
    fontSize: 24,
    flex: 1,
    flexDirection: 'row',
    minHeight: screen.height * 0.15,


  },
  input_style: {

    padding: 5,
    color: '#EF6C00',
    marginTop: 15,
    borderWidth: 1,
    borderColor: '#EF6C00',
    borderRadius: 10,
    width: screen.width * 0.8,
  },
  login_button: {
    minHeight: screen.height * 0.10,
    width: screen.width * 0.8,
    marginTop: '20%',
    padding: 3,
    borderWidth: 1,
    borderColor: '#EF6C00',
    borderRadius: 10,
    



  },
  login_title: {
    
    color: '#EF6C00',
    width: screen.width * 0.8,
    maxHeight: screen.height * 0.10,
    textAlign: 'center',
    fontSize: 30,
    borderBottomWidth: 1,
    borderBottomColor: '#EF6C00',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    
    

  }
});