import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { Component } from 'react'
import { View, Text, StyleSheet,TouchableOpacity } from 'react-native'

import { connect } from 'react-redux'
import { Dimensions } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
// Importing Waves
import BottomWave from '../assets/svg_pats/bottom_wave';

 



class Ayuda extends Component {

  render() {
   
    return (
      <View style={styles.container}>
        <Text style={styles.text_content}>
          {'Usted cuenta con diferentes recursos de ayuda tales como:'}
        </Text>
        <View style={styles.icon_help_resourses}>
          <TouchableOpacity style={{marginLeft:30}} onPress={()=>{alert('click Button help!')}}>
          <AntDesign name="earth" size={30} color="#EF6C00" />

          </TouchableOpacity>
          <TouchableOpacity style={{marginLeft:30}} onPress={()=>{alert('click Button help!')}}>
          <AntDesign name="google" size={30} color="#EF6C00" />

          </TouchableOpacity>
          <TouchableOpacity style={{marginLeft:30}} onPress={()=>{alert('click Button help!')}}>
          <AntDesign name="gitlab" size={30} color="#EF6C00" />

          </TouchableOpacity>
          <TouchableOpacity style={{marginLeft:30}} onPress={()=>{alert('click Button help!')}}>
          <AntDesign name="youtube" size={30} color="#EF6C00" />

          </TouchableOpacity>

        </View>
        <BottomWave />
      </View>
    )
  }
}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(Ayuda)

// Styles
const window = Dimensions.get('window');
const screen = Dimensions.get('screen');
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    position: 'relative',
    width: screen.width,
    height: screen.height,
    minHeight:screen.height*0.8,
  },
  text_content: {
    fontSize: 30,
    textAlign: 'center',
    color: '#FFFFFF',
    fontFamily: 'Raleway_Light',
    marginTop: 30,
    height: screen.height*0.2,
    width: screen.width,
  },
  icon_help_resourses:{
    flex:1,
    flexDirection:'row',
    justifyContent: 'center',
    paddingTop:50,
    
    width:screen.width,
    height:50,
  }
});
