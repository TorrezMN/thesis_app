import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableWithoutFeedback, TouchableOpacity, ImageBackground, Button, Image } from 'react-native'

import { connect } from 'react-redux'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
const bg_image = require('../assets/img/bache.jpg');
const logo_image = require('../assets/img/baches_icon.png');
import { Dimensions } from 'react-native';




const window = Dimensions.get('window');
const screen = Dimensions.get('screen');
 
// Importing Components
import TopWave from '../assets/svg_pats/top_wave';
import BottomWave from '../assets/svg_pats/bottom_wave';
class Index extends Component {
  
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <ImageBackground source={bg_image} style={styles.image}>
          <TopWave />
          <TouchableOpacity style={styles.loginScreenButton} onPress={() => navigate('Menu')}>
            <Image style={styles.logo} source={logo_image} />
            <Text style={styles.text}>Baches Collector</Text>
            
          </TouchableOpacity>
          <BottomWave />
        </ImageBackground>
      </View>
    )
  }
}
const mapStateToProps = (state) => ({
})
const mapDispatchToProps = {
}
export default connect(mapStateToProps, mapDispatchToProps)(Index)
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    width:screen.width,
    height:screen.height,
    minHeight:screen.height*0.75,
  },
  loginText: {
    color: '#fff',
    textAlign: 'center',
    paddingLeft: 10,
    paddingRight: 10
  },
  image: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    resizeMode: "cover",
    justifyContent: "center"
  },
  text: {
    color: "#EF6C00",
    fontSize: 20,
    fontFamily: 'Raleway_Light',
    textAlign: "center",
  },
  
  loginScreenButton: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    width: 100,
    height: 100,
  },
});
