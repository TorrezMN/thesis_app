import * as React from "react"
import Svg, { Path } from "react-native-svg"
import { StyleSheet  } from 'react-native'

import { Dimensions } from 'react-native';




const window = Dimensions.get('window');
const screen = Dimensions.get('screen');
 


function TopWave(props) {
  return (
    <Svg xmlns="http://www.w3.org/2000/svg" style={styles.svg_path}  viewBox="0 0 1440 320" {...props}>
      <Path
        fill="#EF6C00"
        d="M0 32l21.8 26.7C43.6 85 87 139 131 165.3c43.5 26.7 87 26.7 131 21.4 43.5-5.7 87-15.7 131-26.7 43.4-11 87-21 131-10.7 43.3 10.7 87 42.7 131 69.4 43.2 26.3 87 48.3 130 58.6 44.1 10.7 88 10.7 131 5.4 44-5.7 88-15.7 131-10.7 43.9 5 88 27 131 32 43.8 5 87-5 131-5.3 43.7.3 87 10.3 109 16l22 5.3V0H0z"
      />
    </Svg>
  )
}

export default TopWave


const styles = StyleSheet.create({
  svg_path:{
    width:screen.width,
    height:screen.height*0.2,
    
    left: 0,
    top:'-5%',
    overflow:'hidden',
    position:'absolute',
  }
})