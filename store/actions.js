import {
    FORM_NEXT_STEP,
    FORM_PREVIOUS_STEP,
    RESET_FORM,
    RESET_FORM_DATA,
    REGISTRO_FORM_SET_TIPO_CAMINO,
    REGISTRO_FORM_SET_TIPO_MATERIAL,
    REGISTRO_FORM_SET_TIPO_TRAFICO,
    REGISTRO_FORM_SET_QUANTITY,
    REGISTRO_FORM_SET_DEPTH,
    REGISTRO_FORM_SET_DIAMETER,
    REGISTRO_FORM_SET_COORDINATES,
    REGISTER_USER,
    LOGIN_USER,
    REGISTRO_FORM_SAVE,
    
} from './actionTypes';



// Form
export const nextStep = () => ({
    type: FORM_NEXT_STEP,
})
export const prevStep = () => ({
    type: FORM_PREVIOUS_STEP,
})
export const resetForm = () => ({
    type: RESET_FORM,
})
export const resetFormData = () => ({
    type: RESET_FORM_DATA,
})
// Set Form Values
export const registroFormSetTipoCamino = (val) => ({
    type: REGISTRO_FORM_SET_TIPO_CAMINO,
    val
})
export const registroFormSetTipoMaterial = (val) => ({
    type: REGISTRO_FORM_SET_TIPO_MATERIAL,
    val
})
export const registroFormSetTipoTrafico = (val) => ({
    type: REGISTRO_FORM_SET_TIPO_TRAFICO,
    val
})
export const registroFormSetQuantity = (val) => (
    {
        type: REGISTRO_FORM_SET_QUANTITY,
        val,
    })
export const registroFormSetDepth = (val) => (
    {
        type: REGISTRO_FORM_SET_DEPTH,
        val,
    })
export const registroFormSetDiameter = (val) => (
    {
        type: REGISTRO_FORM_SET_DIAMETER,
        val,
    })
export const registroFormSetCoordinates = (val) => (
    {
        type: REGISTRO_FORM_SET_COORDINATES,
        val,
    })
export const registroFormSave = (nav,user) => (
    
    {
        type: REGISTRO_FORM_SAVE,
        nav,
        user,
    })


// REGISTRATION

export const register_new_user = (val,nav) => ({
    type:REGISTER_USER,
    val,
    nav,
})
export const login_user = (val,nav) => ({
    type:LOGIN_USER,
    val,
    nav,
})