import React, { Component } from 'react';
import { View, Text,Dimensions } from 'react-native';
import { Picker } from '@react-native-picker/picker';
import { connect, useSelector, useDispatch } from 'react-redux'
import { registroFormSetTipoCamino } from '../../store/actions'


// Styles
const window = Dimensions.get('window');
const screen = Dimensions.get('screen');


class Parte_0 extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      pickerValue: null,
      
    };
  }
  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
        <Text style={{ fontFamily: 'Raleway_Light', color: '#FFFFFF', fontSize: 25, textAlign: 'center', marginTop: 30 }}> Tipo de Camino </Text>
        <Text style={{ fontFamily: 'Raleway_Light', color: '#FFFFFF', fontSize: 20, textAlign: 'center', marginBottom: 30 }}>{'Seleccione el tipo de camino que corresponda.'} </Text>
        <View style={{ borderWidth: 1, borderColor: '#EF6C00', borderRadius: 5 }}>

          <Picker
            style={{
              height: 50,
              width: screen.width*0.7,
              color: 'white',
              fontFamily: 'Raleway_Light',


            }}


            selectedValue={(this.state && this.state.pickerValue) || '0'}




            onValueChange={(itemValue, itemIndex) => {

              this.props.setTipoDeCamino(itemValue);
              
              this.setState({pickerValue: itemValue});
            }
            }
          >

            <Picker.Item label="< Seleccione Uno >" value="0" />
            <Picker.Item label="Ruta" value="1" />
            <Picker.Item label="Vecinal" value="2" />
            <Picker.Item label="Nacional" value="3" />
          </Picker>
        </View>
      </View>
    );
  }
}

// const mapStateToProps = (state) => ({
//   formulario_registro_nuevo: state.formulario_registro_nuevo,
// })

const mapDispatchToProps = (dispatch, val) => {
  return {
    setTipoDeCamino: (val) => dispatch(registroFormSetTipoCamino(val)),



  }
};

export default connect(null, mapDispatchToProps)(Parte_0)