import React, { Component } from 'react';
import { AntDesign } from '@expo/vector-icons';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
// Importing Waves
import BottomWave from '../assets/svg_pats/bottom_wave';
// Styles
const window = Dimensions.get('window');
const screen = Dimensions.get('screen');






class BacheRegistroCorrecto extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <View style={styles.check_icon_container}>
                <AntDesign name="checkcircleo" size={100} color='#2E7D32'/>

                </View>
                <Text style={styles.page_title}>{' Exito! - Se registro la ubicacion.'} </Text>
                <Text style={styles.page_descripcion}> {'Se ha registrado correctamente el bache.'} </Text>

                <TouchableOpacity style={styles.button_aceptar} onPress={() => navigate('Menu')}>
                    <Text style={styles.button_text}>Aceptar</Text>
                </TouchableOpacity>
                
                <BottomWave />
            </View>
        );
    }
}

export default BacheRegistroCorrecto;



const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        width: screen.width,
        height: screen.height,
    },
    check_icon_container:{
        marginTop:'-20%',
        minWidth:screen.width*0.60,
        maxWidth:screen.width*0.60,
        minHeight:screen.height*0.30,
        maxHeight:screen.height*0.30,
        marginBottom:5,
        flex:1,
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'center',

    },
    page_title: {
        width: screen.width * 0.80,
        color: '#FFFFFF',
        backgroundColor: '#2E7D32',
        padding: 5,
        fontFamily: 'Raleway_Light',
        textAlign: 'center',
        fontSize: 20,
    },
    page_descripcion: {
        textAlign: 'left',
        fontFamily: 'Raleway_Light',
        marginTop: 4,
        width: screen.width * 0.80,
        fontSize: 15,
        color: '#FFFFFF',
    },
    button_text:{
        color:'#EF6C00',
        textAlign:'center',
    },
    button_aceptar:{
        flex:1,
        flexDirection:'column',
        justifyContent:'center',
        alignItems:'center',
        marginTop:'5%',
        borderWidth:1,
        borderColor:'#EF6C00',
        borderRadius:5,
        padding: 5,
        minWidth:screen.width*0.40,
        maxHeight:screen.height*0.10,
        zIndex:100,
    }
})