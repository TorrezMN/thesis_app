import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
// Importing Waves
import BottomWave from '../assets/svg_pats/bottom_wave';
// Styles
const window = Dimensions.get('window');
const screen = Dimensions.get('screen');






class ErrorGenericoRegistroUsuarios extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <Text style={styles.page_title}> Error - Registro de Usuarios </Text>
                <Text style={styles.page_descripcion}> {'Ocurrio Un problema con el registro. Intente nuevamente.'} </Text>

                <TouchableOpacity style={styles.button_aceptar} onPress={() => navigate('Register')}>
                    <Text style={styles.button_text}>Aceptar</Text>
                </TouchableOpacity>
                <BottomWave />
            </View>
        );
    }
}

export default ErrorGenericoRegistroUsuarios;



const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        width: screen.width,
        height: screen.height,
    },
    page_title: {
        width: screen.width * 0.80,
        color: '#FFFFFF',
        backgroundColor: 'red',
        padding: 5,
        fontFamily: 'Raleway_Light',
        textAlign: 'left',
    },
    page_descripcion: {
        textAlign: 'left',
        fontFamily: 'Raleway_Light',
        marginTop: 4,
        width: screen.width * 0.80,
        color: '#FFFFFF',
    },
    button_text:{
        color:'#EF6C00',
        textAlign:'center',
    },
    button_aceptar:{
        flex:1,
        flexDirection:'column',
        justifyContent:'center',
        alignItems:'center',
        marginTop:'10%',
        borderWidth:1,
        borderColor:'#EF6C00',
        borderRadius:5,
        padding: 5,
        minWidth:screen.width*0.40,
        maxHeight:screen.height*0.10,
    }
})