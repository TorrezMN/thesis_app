import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { connect, useSelector, useDispatch } from 'react-redux';
import { nextStep, prevStep, resetForm, registroFormSetCoordinates, resetFormData, registroFormSave } from '../store/actions';
import PropTypes from 'prop-types';

import { AntDesign } from '@expo/vector-icons';
import { Foundation } from '@expo/vector-icons';
import { Dimensions } from 'react-native';
// Importing Waves
import BottomWave from '../assets/svg_pats/bottom_wave';



class Menu extends Component {
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.menu_container}>
        <View style={styles.menu}>

          <TouchableOpacity style={styles.loginScreenButton} onPress={() => navigate('Estadisticas')}>
            <AntDesign name="barschart" size={30} color="#EF6C00" />
            <Text style={{ color: '#EF6C00', fontFamily: 'Raleway_Light' }}>Estadisticas</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.loginScreenButton} onPress={() => {this.props.reset_Form_Data();this.props.reset_Form();navigate('NuevoRegistro')}}>
            <AntDesign name="addfolder" size={30} color="#EF6C00" />
            <Text style={{ color: '#EF6C00', fontFamily: 'Raleway_Light' }}>Nuevo Registro</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.loginScreenButton} onPress={() => navigate('Config')}>
            <AntDesign name="setting" size={30} color="#EF6C00" />
            <Text style={{ color: '#EF6C00', fontFamily: 'Raleway_Light' }}>Configuracion</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.loginScreenButton} onPress={() => navigate('Ayuda')}>
            <AntDesign name="questioncircleo" size={30} color="#EF6C00" />
            <Text style={{ color: '#EF6C00', fontFamily: 'Raleway_Light' }}>Ayuda</Text>
          </TouchableOpacity>

        </View>
        <BottomWave />
      </View>
    )
  }
}
const mapStateToProps = (state) => ({
})
const mapDispatchToProps = (dispatch, val) => {
  return {
  
    reset_Form_Data: () => dispatch(resetFormData()),
  reset_Form: () => dispatch(resetForm()),
  }
};
export default connect(mapStateToProps, mapDispatchToProps)(Menu)
// Styles
const window = Dimensions.get('window');
const screen = Dimensions.get('screen');

const styles = StyleSheet.create({
  menu_container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    position: 'relative',
    width: screen.width,
    height: screen.height,
    minHeight:screen.height*0.8,
  },
  menu: {
    top: -screen.height * 0.1,

  },
  loginScreenButton: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '10%',
  }
});