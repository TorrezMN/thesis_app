import React, { Component } from 'react'
import { StyleSheet, Text, View, Image, Dimensions } from 'react-native'

// Styles
const window = Dimensions.get('window');
const screen = Dimensions.get('screen');
const activity_image = require('../assets/img/activity.gif');

// Importing Waves
import BottomWave from '../assets/svg_pats/bottom_wave';


class ActivityIndicator extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Image style={styles.tinyLogo} source={activity_image} />
                <BottomWave />
            </View>
        )
    }
}

export default ActivityIndicator


const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
        width: screen.width,
        height: screen.height,
    },
    tinyLogo: {
        width: screen.width,
        height: screen.height,


    }
}
)