import * as React from "react"
import Svg, { Path } from "react-native-svg"
import { StyleSheet  } from 'react-native'
import { Dimensions } from 'react-native';




const window = Dimensions.get('window');
const screen = Dimensions.get('screen');
 


function BottomWave(props) {
  return (
    <Svg xmlns="http://www.w3.org/2000/svg" style={styles.svg_path}  viewBox="0 0 1440 320" {...props}>
      <Path
        fill="#EF6C00"
        d="M0 32l21.8 5.3C43.6 43 87 53 131 85.3 174.5 117 218 171 262 192c43.5 21 87 11 131-16 43.4-27 87-69 131-90.7C567.3 64 611 64 655 96c43.2 32 87 96 130 138.7 44.1 42.3 88 64.3 131 42.6 44-21.3 88-85.3 131-122.6 43.9-37.7 88-47.7 131-42.7 43.8 5 87 27 131 32 43.7 5 87-5 109-10.7l22-5.3v192H0z"
      />
    </Svg>
  )
}

export default BottomWave


const styles = StyleSheet.create({
  svg_path:{
    width:screen.width,
    height:screen.height*0.2,
    
    left: 0,
    overflow:'hidden',
    position:'absolute',
    bottom:'-7%',
    
  }
})