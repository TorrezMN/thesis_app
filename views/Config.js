import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { AntDesign } from '@expo/vector-icons';
import { MaterialIcons } from '@expo/vector-icons';
import { Dimensions } from 'react-native';
// Importing Waves
import BottomWave from '../assets/svg_pats/bottom_wave';



class Menu extends Component {
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.menu_container}>
        <View style={styles.menu}>

          <TouchableOpacity style={styles.loginScreenButton} onPress={() => navigate('Login')}>
            <AntDesign name="login" size={30} color="#EF6C00" />
            <Text style={{ color: '#EF6C00', fontFamily: 'Raleway_Light' }}>Login</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.loginScreenButton} onPress={() => navigate('Register')}>
            <MaterialIcons name="app-registration" size={30} color="#EF6C00" />
            <Text style={{ color: '#EF6C00', fontFamily: 'Raleway_Light' }}>Registrarse</Text>
          </TouchableOpacity>
         
        </View>
        <BottomWave />
      </View>
    )
  }
}
const mapStateToProps = (state) => ({
})
const mapDispatchToProps = {
}
export default connect(mapStateToProps, mapDispatchToProps)(Menu)
// Styles
const window = Dimensions.get('window');
const screen = Dimensions.get('screen');

const styles = StyleSheet.create({
  menu_container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    position: 'relative',
    width: screen.width,
    height: screen.height,
    minHeight:screen.height*0.8,
  },
  menu: {
    top: -screen.height * 0.1,

  },
  loginScreenButton: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '20%',
  }
});